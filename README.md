# CREASOUND #

This is the repository for the Innovation Project Creasound

------------------
## Table of content
* Contribution guidelines
* Library used
* Installations
* Execution
* Team


------------------

### Contribution guidelines ###

* Use english for both commit message and development (names/comments/...)
* Test before you push
* May the force be with you



------------------

### Library used ###

* [Essentia](http://essentia.upf.edu/)
* [Gaia](https://github.com/MTG/gaia)
* [SFML](http://www.sfml-dev.org/)
* [SoundTouch](http://www.surina.net/soundtouch/)

------------------

### Installations

* To install Essentia, simply follow its installation tutorial [here](http://github.com/MTG/essentia).
* To get the SVM models, download with this link : http://essentia.upf.edu/documentation/svm_models/
* To install Gaia (needed for Essentia), simply follow its installation tutorial [here](http://guthub.com/MTG/gaia).
If having troubles with the command `./waf install` while installing Essentia or Gaia try to execute it as super user (i.e. `sudo` on Ubuntu).
* To install SFML follow the tutorial [here](http://www.sfml-dev.org/tutorials/)
* To install SoundTouch download the sources [here](http://www.surina.net/soundtouch/sourcecode.html), extract them in /usr/lib/soundtouch and follow [this tutorial](http://www.surina.net/soundtouch/README.html) and then execute this command : `sudo cp /usr/local/lib/libSoundTouch.* /usr/lib/`

### Execution

------------------
### Team ###

* [Adspartan](https://bitbucket.org/adspartan/)
* [julienandre](https://bitbucket.org/julienandre/)
* [yozamu](https://bitbucket.org/yozamu/)
* [Akuni](https://bitbucket.org/akuni/)
