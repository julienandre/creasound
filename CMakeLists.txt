cmake_minimum_required(VERSION 3.3)
project(creasound)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES src/*.cpp src/*.h src/SVM.cpp src/SVM.h)
add_executable(creasound ${SOURCE_FILES})