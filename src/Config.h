#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>
#include "Playlist.h"

enum GenerationMode
{
    //GM_MULTIPLEXER = 1,
    //GM_RAND = 2,
    GM_MERGE = 1,
    //GM_SWITCH = 4,
    GM_FADE = 2,
    //GM_SVM = 6,
    GM_TONAL = 3,
    GM_CHANGE_PITCH = 9997,
    GM_CHANGE_TEMPO = 9998,
    GM_CHANGE_TEMPO_PC = 9999,
    GM_LAST_SETTING = 99910,
    GM_LOAD_CONF = 4
};

class Config
{
public:
     Config() : tempo(0) 
              , pitch(0) 
              , playlistName("#") 
              , generationMode(0)
              , _playlist(nullptr)
              {}

    float tempo;
    float pitch;
    std::string configName;
    std::string playlistName;
    std::string outName;
    int generationMode;
    
    std::set<std::string> tracks;
    
     
    bool save(std::string file);
    
    
    void changePlaylist(Playlist* p)
    {
        _playlist = p;
        playlistName = p ? p->name() : "#";
    }
    
    Playlist* playlist() const
    {
        return _playlist;
    }

    void reset() {
        tempo=0;
        pitch=0;
        playlistName="#";
        generationMode=0;
        _playlist=nullptr;
        configName="default";
    }

    
    static Config* load(std::string file);
    static std::vector<std::string> listConfig();
    
private:
    Playlist* _playlist;
};

#endif