//
// Created by user on 06/06/16.
//

#include <iostream>
#include "Menus.h"
#include "MusicFactory.h"
#include "Utils.h"
#include "SoundTouchAPI.h"
#include "PlaylistManager.h"

using namespace std;

static void flush_cin()
{
    cin.clear();
    fflush(stdin);
}

static string save_config(Config* conf)
{
    /*char save;
    cout << "Do you want to save the current config ? (y/n) ";
    cin >> save;
    flush_cin();*/
    
    //if(save == 'y' || save == 'Y')
    //{
        string configName;
        cout << "Choose a name: ";
        cin >> configName;
        
        if(!conf->save("config/"+configName+".conf"))
        {
            cerr << "Error: could not save config" << endl;   
        }
        return configName;
    //}
    return "";
}

static void print_gen_choices(Config* conf);

int Menus::menu_main() {
    int choice;
    cout << "\n=========" << endl;
    cout << "Main menu" << endl;
    cout << "=========" << endl;
    cout << "1) Generate a music" << endl;
    cout << "2) Manage playlists" << endl;
    cout << "3) Change generation parameters" << endl;
    cout << "0) Exit the application" << endl;
    cout << "Choice: ";
    cin >> choice;
    flush_cin();
    cout << "\033[2J\033[1;1H";
    switch(choice) {
        case 1:
            menu_generate();
            break;
        case 2:
            menu_playlists();
            break;
        case 3:
            menu_parameters();
            break;
        case 0:
            break;
        default:
            cout << "ERROR: invalid input." << endl;
            break;
    }
    return choice;
}

void Menus::menu_playlists() {
    PlaylistManager* manager = PlaylistManager::getInstance();
    int choice;
    
    do
    {
        cout << "\n==============" << endl;
        cout << "Playlists menu" << endl;
        cout << "==============" << endl;
        cout << "1) Create a new playlist" << endl;
        cout << "2) Add tracks to a playlist" << endl;
        cout << "3) Remove tracks from a playlist" << endl;
        cout << "4) Display the playlists" << endl;
        cout << "5) Display the content of a playlist" << endl;
        cout << "6) Delete a playlist" << endl;
        cout << "7) Rename a playlist" << endl;
        cout << "0) Previous" << endl;
        
        cout << "Choice: ";
        cin >> choice;
        flush_cin();
        cout << "\033[2J\033[1;1H";
        switch(choice) {
            case 1: {
                string playlist_name;
                cout << "Playlist name: " << endl;
                cin >> playlist_name;
                flush_cin();
                manager->addPlaylist(playlist_name, new Playlist(playlist_name));
                break;
            }
            case 2:
                manager->editPlaylist(select_playlist());
                
                break;
            case 3:
                manager->editPlaylist(select_playlist(), false);
                break;
            case 4:
                manager->displayPlaylistList();
                break;
            case 5: {
                string playlist = select_playlist();
                if(manager->playlistExist(playlist))
                    manager->getPlaylist(playlist)->print();
                else
                    cout << playlist << " is not a playlist" << endl;
                break;
            }
            case 6: {
                string playlist = select_playlist();
                if(manager->playlistExist(playlist))
                    manager->removePlaylist(playlist);
                else
                    cout << playlist << " is not a playlist" << endl;
                break;
                break;
            }
            case 7: {
                manager->displayPlaylistList();
                string old_name, new_name;
                cout << "Playlist name: ";
                cin >> old_name;
                flush_cin();
                cout << "New name: ";
                cin >> new_name;
                flush_cin();
                manager->renamePlaylist(old_name,new_name);
                break;
            }
            case 0:
                break;
            default:
                cout << "ERROR: invalid input." << endl;
                break;
        }
        
        manager->saveAll();
    }while(choice);
}

void Menus::menu_parameters() {
    static Config* conf = Config::load("config/last.conf");
    int choice;

    do
    {
        cout << "\n==============" << endl;
        cout << "Parameters menu" << endl;
        cout << "==============" << endl;
        cout << "Settings: " << endl;
        cout << "Configuration name: " << conf->configName << endl;
        cout << "Tempo change: " << conf->tempo << " %" << endl;
        cout << "Pitch change: " << conf->pitch << " semitone" << endl;
        cout << "=====" << endl;
        cout << "1) Change tempo" << endl;
        cout << "2) Change pitch" << endl;
        cout << "3) Reset to default settings" << endl;
        cout << "4) Save configuration" << endl;
        cout << "5) Load configuration" << endl;
        cout << "0) Previous" << endl;

        cout << "Choice: ";
        cin >> choice;
        flush_cin();
        cout << "\033[2J\033[1;1H";
        switch(choice) {
            case 1:
                cout << "Tempo change (%): ";
                cin >> conf->tempo;
                break;
            case 2:
                cout << "Pitch change (in semitone): ";
                cin >> conf->pitch;
                break;
            case 3:
                conf->reset();
                break;
            case 4:
                conf->configName = save_config(conf);
                break;
            case 5:
                conf = load_config();
                break;
            case 0:
                break;
            default:
                cout << "ERROR: invalid input." << endl;
                break;
        }

    }while(choice);
}

void Menus::menu_generate() {
    PlaylistManager* manager = PlaylistManager::getInstance();
    static Config* conf = Config::load("config/last.conf");
    if(conf == nullptr)
        conf = new Config();
    
    int choice;
    string input;
    bool continueGen = false;
    
    do
    {
        if(conf->playlist())
        {
            conf->tracks.clear();
            conf->tracks = conf->playlist()->getRandomTracks(2);
        }
        
        if(!continueGen)
        {
            print_gen_choices(conf);
            
            cout << "Choice: ";
            cin >> choice;
            flush_cin();
            cout << "\033[2J\033[1;1H";
            // if we don't cancel the generation, ask for tracks input
            //if(!conf || choice != GM_LAST_SETTING)
                //conf = new Config();
            
            if(choice != GM_LAST_SETTING && choice != GM_LOAD_CONF)
            {
                conf->generationMode = choice;
            }
            else if(choice == GM_LOAD_CONF)
            {
                conf = load_config();
                // to use the new config
                choice = GM_LAST_SETTING;
            }
            else if(conf == nullptr)
            {
                cout << "No previous settings found" << endl;
                continue;
            }
            
            if(choice > 0)
            {
                char choiceInput;
                
                if (choice != GM_LAST_SETTING && manager->playlistCount() > 0)
                {
                    cout << "Do you want to use a playlist ? (y/n) ";
                    cin >> choiceInput;
                    flush_cin();
                }       
                    
                if((choiceInput == 'y' || choiceInput == 'Y') && choice != GM_LAST_SETTING)
                {
                    conf->changePlaylist(manager->getPlaylist(select_playlist()));
                }        
                
                if(choice != 0 && choice != GM_LAST_SETTING) 
                {                
                    if(conf->playlist())
                    {
                        if(conf->playlist()->trackCount() > 1)
                            conf->tracks = conf->playlist()->getRandomTracks(2);
                        else
                            cout << conf->playlistName << " has less than 2 tracks" << endl;
                    }
                    
                    if(conf->tracks.size() < 2)
                    {    
                        do
                        {
                            cout << "Track" << (conf->tracks.size()+1) << " path: ";
                            cin >> input;
                            flush_cin();
                            if(Utils::is_valid_music(input))
                                conf->tracks.insert(input);
                        }while(conf->tracks.size() < 2);
                    }
                }
            }
        }

        if(choice==GM_MERGE || choice==GM_FADE || choice==GM_TONAL) {
            stringstream out;

            out << "music_" << rand() << ".mp3";
            conf->outName = out.str();

            cout << "Name=" << conf->outName << endl;
        }
            
        conf->save("config/last.conf");
        
        auto it = conf->tracks.begin();
        
        
        switch(conf->generationMode) {
            case 0:
                break;
            case GM_MERGE:
                continueGen = MusicFactory::createMusic(*it, *std::next(it, 1), conf->outName, MF_ALGO_MERGE, conf);
                break;
            case GM_FADE:
                continueGen = MusicFactory::createMusic(*it, *std::next(it, 1), conf->outName, MF_CUSTOM, conf);
                break;
            case GM_TONAL:
                continueGen = MusicFactory::createMusic(*it, *std::next(it, 1), conf->outName, MF_TONAL, conf);
                break;
            default:
                cout << "ERROR: invalid input." << endl;
                break;
        } 

        // we won't use tracks now...
        if(!continueGen) 
            conf->tracks.clear();
        
    }while(choice);    
}


string Menus::select_playlist()
{
    cout << "Playlists: " << endl;
    PlaylistManager::getInstance()->displayPlaylistList();
    string playlist_name;
    cout << "Select a playlist: ";
    cin >> playlist_name;
    flush_cin();
    
    return playlist_name;
}

Config* Menus::load_config()
{   
    vector<string> configs = Config::listConfig();
    
    if(!configs.size())
    {
        cout << endl;
        cout << "[INFO] : no configuration available" << endl;
        // return default/empty config
        return new Config();
    }
    cout << "Available configurations:" << endl;
    for(const auto& s : configs)
        cout << " - " << s << endl;
    string config;
    cout << "Select a configuration: ";
    cin >> config;
    
    auto it = find(configs.begin(), configs.end(), config);
    
    if(it == configs.end())
    {
        cout << "Error: unknown config";
        return load_config();
    }
    else
    {
        return Config::load("config/"+config+".conf");
    }
}

void print_gen_choices(Config* conf)
{
    cout << "\n===============" << endl;
    cout << "Generation menu" << endl;
    cout << "===============" << endl;
    cout << "Settings: " << endl;
    cout << "Configuration name: " << conf->configName << endl;
    cout << "Tempo change: " << conf->tempo << " %" << endl;
    cout << "Pitch change: " << conf->pitch << " semitone" << endl;
    cout << "=====" << endl;
    //cout << GM_MULTIPLEXER << ") Multiplexer method" << endl;
    //cout << GM_RAND << ") Rand method" << endl;
    cout << GM_MERGE << ") Merge method" << endl;
    //cout << GM_SWITCH << ") Switch method" << endl;
    cout << GM_FADE << ") Fade method" << endl;
    cout << GM_TONAL << ") Tonal method" << endl;
    //cout << GM_CHANGE_PITCH << ") Change pitch" << endl;
    //cout << GM_CHANGE_TEMPO << ") Change tempo(value)" << endl;
    //cout << GM_CHANGE_TEMPO_PC << ") Change tempo(%)" << endl;
    //cout << GM_LAST_SETTING << ") Use last settings" << endl;
    cout << GM_LOAD_CONF << ") Load configuration" << endl;
    cout << "0) Previous" << endl;   
}