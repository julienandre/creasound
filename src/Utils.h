#ifndef CREASOUND_UTILS_H
#define CREASOUND_UTILS_H

#include <iostream>
#include <set>

enum AskSaveReturn
{
    ASR_YES  = 0,
    ASR_NO   = 1,
    ASR_STOP = 2  
};

class Utils {    
public:
    static bool is_valid_music (const std::string& name);
    static int ask_save(std::string filename);
    static bool good_music_extension(const std::string &name);
    static char separator();
    static void process_directory( const std::string &dirName, int (*fct)(const std::string&));
    static bool file_exist(const std::string& name);
    static size_t sec_to_frame(const float sec, const int sampleRate = 44100);
    static void changeTempoValue(std::string in, std::string out, int value);
    static std::string extract_filename(std::string path);
};


#endif //CREASOUND_UTILS_H
