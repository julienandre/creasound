//
// Created by SARROCHE on 07/06/16.
//

#ifndef CREASOUND_LEARNING_H
#define CREASOUND_LEARNING_H


#include <essentia/pool.h>
#include <gaia2/gaia.h>
#include <gaia.h>
#include <dataset.h>
#include <point.h>
#include <parameter.h>
#include "Utils.h"


class Learning {

private:

    gaia2::PointLayout _layout;
    gaia2::DataSet * _d;

public:



    void init(){
        /*if(_layout != NULL) return;*/
        _layout.add("bpm" , gaia2::StringType);
        _layout.add("key" , gaia2::StringType);
        _layout.add("kind", gaia2::StringType);

        /*if(_d != NULL) return;*/
        _d = new gaia2::DataSet();
        if(Utils::file_exist("generated/database/test.db"))
            _d->load("generated/database/test.db", 0, 100);
    }


    bool store_descriptors(bool kind, std::string filename);
    bool add_point_svm(bool kind, std::string filename, gaia2::DataSet * d, const char* bpm, const char* key);

    bool train();
    gaia2::PointLayout getLayout(){ return _layout; }
    gaia2::DataSet * getDataSet(){ return _d; }
    static void addSVMDescriptors(essentia::Pool &pool);

};


#endif //CREASOUND_LEARNING_H
