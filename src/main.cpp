#include <iostream>
#include <gaia.h>

#include "Utils.h"
#include "Playlist.h"
#include "MusicFactory.h"
#include "Menus.h"
#include "Config.h"

using namespace std;


int main(int argc, char *argv[]) {
    int choice = 1;

    cout << "\033[2J\033[1;1H";
    cout << "==================================" << endl;
    cout << "| Creasound, sound of the future |" << endl;
    cout << "==================================" << endl;

    srand(time(0));
    essentia::init();
    gaia2::init();

    while(choice!=0) {
        choice = Menus::menu_main();
    }
    
    gaia2::shutdown();
    essentia::shutdown();

    return 0;
}