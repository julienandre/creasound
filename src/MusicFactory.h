#ifndef MUSIC_FACTORY_H
#define MUSIC_FACTORY_H

#include <iostream>
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include "Config.h"

#define IN 0
#define OUT 1

enum MusicFactoryAlgorithm
{
    MF_ALGO_MERGE   = 0,
    MF_ALGO_SWITCH  = 1,
    MF_ALGO_RAND    = 2,
    MF_CUSTOM       = 3,
    MF_MULTIPLEXER  = 4,
    MF_TONAL        = 5,
    MF_SVM          = 6
};

class MusicFactory
{
public:    
    static bool createMusic( std::string f1
                           , std::string f2
                           , std::string out
                           , MusicFactoryAlgorithm algo
                           , Config* conf);
    static void saveMusic( std::vector<essentia::Real> res
                         , std::string out
                         , essentia::Real sampleRate);

    static void _mkdir(const char *dir);
    
private:
    static std::vector<essentia::Real> randShuffle( essentia::standard::Algorithm* fact1
                                                  , essentia::standard::Algorithm* fact2);
    static std::vector<essentia::Real> switchShuffle(essentia::standard::Algorithm* fact1
                                                  , essentia::standard::Algorithm* fact2);
    static std::vector<essentia::Real> merge( essentia::standard::Algorithm* fact1
                                            , essentia::standard::Algorithm* fact2);

    static std::vector<essentia::Real> switchWithTonalDetection( essentia::standard::Algorithm* fact1,
                                                                 essentia::standard::Algorithm* fact2,
                                                                 std::string filename1,
                                                                 std::string filename2 );

    static std::vector<essentia::Real> switchWithSVM( essentia::standard::Algorithm* fact1,
                                                                       essentia::standard::Algorithm* fact2,
                                                                       std::string filename1,
                                                                       std::string filename2 );

    static std::vector<essentia::Real> randWithFadeDetection( essentia::standard::Algorithm* fact1,
                                                        essentia::standard::Algorithm* fact2,
                                                       std::string filename1,
                                                       std::string filename2 );
                                                       
    static std::vector<essentia::Real> multiplexer( essentia::standard::Algorithm* fact1
                                            , essentia::standard::Algorithm* fact2);
                                                       
    static void equalize_loudness(std::vector<essentia::Real>& v1, std::vector<essentia::Real>& v2);

};


#endif