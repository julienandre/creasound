//
// Created by julien on 08/06/16.
//

#ifndef CREASOUND_DESCRIPTOR_H
#define CREASOUND_DESCRIPTOR_H

#include <iostream>
#include "streaming_extractor_music.h"
#include "Utils.h"
#include <sys/stat.h>
#include <dirent.h>

using namespace std;

class Descriptor {
private:
    static string _profile_path(){return "profiles/all_config.yaml";}

    static int _process_file_audio_file_to_low_level(const string& file_path, const string& plain_file_name){
        if(Utils::file_exist(StreamingExtractorMusic::LL_dir() +
                             plain_file_name +
                             "_LL.json"))
            return 0;

        return StreamingExtractorMusic::Low_level(file_path,
                                           StreamingExtractorMusic::LL_dir() +
                                                   plain_file_name +
                                                   "_LL.json",
                                                  _profile_path());
    }

    static int _process_file_low_to_high_level(const string& plain_file_name){
        if(Utils::file_exist(StreamingExtractorMusic::HL_dir() +
                             plain_file_name +
                             "_HL.json"))
            return 0;

        return StreamingExtractorMusic::High_level(StreamingExtractorMusic::LL_dir() +
                                                    plain_file_name +
                                                    "_LL.json",
                                            StreamingExtractorMusic::HL_dir() +
                                                    plain_file_name +
                                                    "_HL.json",
                                                   _profile_path());
    }

    static int _process_file(const string& file_path){
        int err;
        if(!Utils::good_music_extension(file_path)){ // if not a music
            return 2;
        }

        int separator_index, point_index;
        separator_index = file_path.find_last_of(Utils::separator())+1; // after folders .../plain_file_name.mp3, ...
        point_index = file_path.find_last_of("."); // before .mp3, .wav, ...

        string plain_file_name = file_path.substr(
                separator_index, // from last separator
                point_index - separator_index); // to last point

        err = _process_file_audio_file_to_low_level(file_path, plain_file_name);

        if(err != 0) // error occurred when processing audio file to low level
            return 1;

        // high level requires low level
        return _process_file_low_to_high_level(plain_file_name);
    }

public:
    static void show_usage(){
        cout << "Usage : musicdir1 musicdir2 ..." << endl;
    }

    static void process_dir(const string& dir_path){
        Utils::process_directory(dir_path,&_process_file);
    }
};

#endif //CREASOUND_DESCRIPTOR_H
