//
// Created by SARROCHE on 07/06/16.
//

#include "Learning.h"
#include <essentia/algorithmfactory.h>
#include <gaia2/analyzer.h>

using namespace std;
using namespace essentia;
using namespace standard;
using namespace essentia::streaming;
using namespace essentia::scheduler;
using namespace gaia2;

void Learning::addSVMDescriptors(Pool &pool) {
    //const char* svmModels[] = {}; // leave this empty if you don't have any SVM models
    const char *svmModels[] = {"BAL", "CUL", "GDO", "GRO", "GTZ", "PS", "VI",
                               "MAC", "MAG", "MEL", "MHA", "MPA", "MRE", "MSA"};

    string pathToSvmModels = "svm_models/";

    for (int i = 0; i < (int) ARRAY_SIZE(svmModels); i++) {
        string modelFilename = pathToSvmModels + string(svmModels[i]) + ".model";
        standard::Algorithm *svm = standard::AlgorithmFactory::create("SvmClassifier",
                                                                      "model", modelFilename);

        svm->input("pool").set(pool);
        svm->output("result").set(pool);
        svm->compute();

        delete svm;
    }
}


/*PointLayout getLayout(){
    return Learning::_layout;
}*/


bool Learning::store_descriptors(bool kind, std::string filename){
    // get filename
    std::string name = Utils::extract_filename(filename);
    // get prefix filename
    std::string kind_str = (kind)? "good_": "bad_";

    // compute output files name
    const char *output1 = std::string("generated/descriptors/lowlevel/"+ kind_str + name + ".json").c_str();
    const char *output2 = std::string("generated/descriptors/highlevel/" + kind_str + name + ".json").c_str();

    // move files
    rename("generated/descriptors/highlevel/bad_tmp.json", output2);
    rename("generated/descriptors/lowlevel/bad_tmp.json", output1);

    return true;
}


bool Learning::add_point_svm(bool kind, std::string filename, gaia2::DataSet * d, const char* bpm, const char* key){
    // get filename
    std::string name = Utils::extract_filename(filename);

    // init point
    Point p;

    // set layout (BPM , KEY, KIND)
    PointLayout l = _layout;
    p.setLayout(l);



    // set name according to point kind
    if(kind){
        p.setName(("GOOD_" + name).c_str());
    }else {
        p.setName(("BAD_" + name).c_str());
    }
    // set BPM
    QString qBpm(bpm);
    p.setLabel("bpm", qBpm);

    // init QStrings
    const char* kind_str   = (kind)? "GOOD" : "BAD";
    QString qKey(key);
    QString qKind(kind_str);

    // set labels
    p.setLabel("key", qKey);
    p.setLabel("kind", qKind);

    // add new point to dataset
    _d->addPoint(&p);

    // store new database
    _d->save("generated/database/test.db");
    return true;
}


bool Learning::train(){

    gaia2::ParameterMap params;
    params.insert("descriptorNames", this->getDataSet()->layout().descriptorNames(StringType));
    gaia2::DataSet* dse = transform(this->getDataSet(), "enumerate", params);


    gaia2::ParameterMap params2;
    params2.insert("className", "bpm");
    params2.insert("descriptorNames", this->getDataSet()->layout().descriptorNames(StringType));
    gaia2::DataSet* dse666 = transform(dse, "svmtrain", params2);

    /*gaia2::SVMTrain svmTrain(*params2);
    svmTrain.analyze(dse);*/

    return true;
}