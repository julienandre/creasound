#ifndef PLAYLIST_MANAGER_H
#define PLAYLIST_MANAGER_H

#include <iostream>
#include <string>
#include <unordered_map>
#include "Playlist.h"

typedef std::unordered_map<std::string, Playlist*> PlaylistMap;

class PlaylistManager
{
public:
    static PlaylistManager* getInstance(){
        static PlaylistManager instance;
        return &instance;
    }
    
    bool playlistExist(const std::string& name) const
    {
        return (_playlists.find(name) != _playlists.end());
    }
    
    Playlist* getPlaylist(const std::string& name) const
    {
        return (playlistExist(name) ? _playlists.at(name) : nullptr);
    }
    
    void addPlaylist(std::string name, Playlist* p)
    {
        _playlists[name] = p;
    }
    
    void removePlaylist(std::string name);
    
    void renamePlaylist(std::string name, std::string newName);
    
    void saveAll()
    {
        for(auto it = _playlists.begin(); it != _playlists.end(); it++)
        {
            Playlist* p = it->second;
            if(p->changed)
                p->save();
        }
    }
    
    void displayPlaylistList();
    
    void editPlaylist(std::string name, bool add = true);
    
    int playlistCount() const
    {
        return _playlists.size();
    }
    

private:
     void loadPlaylists();

    PlaylistManager() {
        loadPlaylists();
    }  
    
    PlaylistMap _playlists;
    
};

#endif