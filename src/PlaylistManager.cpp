#include "PlaylistManager.h"
#include "dirent.h"

void PlaylistManager::removePlaylist(std::string name)
{
    auto it = _playlists.find(name);
    if(it != _playlists.end())
        _playlists.erase(it);
    remove(("generated/playlists/"+name+".txt").c_str());
}

void PlaylistManager::loadPlaylists()
{
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir ("generated/playlists/")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            std::string name(ent->d_name);
            if(name == "" || name == "." || name == "..")
                continue;
            name = name.substr(0, name.find_last_of("."));
            addPlaylist(name, new Playlist(name));
        }
        closedir(dir);
    }
}

void PlaylistManager::renamePlaylist(std::string name, std::string newName)
{
    auto it = _playlists.find(name);
    if (it != _playlists.end()) {
        std::swap(_playlists[newName], it->second);
        _playlists[newName]->rename(newName);
        removePlaylist(name);
    }
}

void PlaylistManager::displayPlaylistList()
{
    for(auto it : _playlists)
        std::cout << "- " << it.first << std::endl;
}

void PlaylistManager::editPlaylist(std::string name, bool add)
{
    std::string track = "stop";
    
    if(!playlistExist(name))
    {
        std::cout << name << " is not a playlist" << std::endl;
        return;   
    }
    
    Playlist* playlist = _playlists[name];
    
    do
    {
        std::cout << "Track list:" << std::endl;
        playlist->print();
        std::cout << "Select a track (or type \"stop\"): ";
        std::cin >> track;
        
        if(track=="stop") 
            break;
        
        if(!add)
            playlist->removeTrack(track);
        else if(Utils::is_valid_music(track))
            playlist->addTrack(track);
        else
            std::cout << "Unknown track" << std::endl;
            
    }while(track!="stop");
}

