//
// Created by user on 02/06/16.
//

#ifndef CREASOUND_EXTRACTOR_H
#define CREASOUND_EXTRACTOR_H

#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <vector>

#define IN 0
#define OUT 1

class Extractor {
private:
     static const std::string LL_DIR;
     static const std::string HL_DIR;
     static const std::string PROFILE_DIR;

public:
    static essentia::Pool compute_tonal_information(std::string name);
    static essentia::Pool compute_rythm_information(std::string name);

    static int compute_lowlevel_information(std::string audioFilename,
                                            std::string lowLevelJsonOutput="bad_tmp",
                                            std::string profileFilename=PROFILE_DIR + "all_config.yaml");
    static essentia::Pool compute_highlevel_information(std::string highLevelJsonInput,
                                             std::string highLevelJsonOutput="bad_tmp",
                                             std::string profileFilename=PROFILE_DIR + "all_config.yaml");

    static essentia::Pool compute_fades_information(std::string name);
    static std::vector<std::vector<essentia::Real>> compute_phrases_from_fades(std::string name);

    static int compute_all_information(std::string name, std::string profileFilename="");

    template <typename T>
    static T extract_from_pool(essentia::Pool pool, const char* keyword){
        T res{};
        // check if the pool contains the keyword
        if(pool.contains<T>(keyword)){
            // retrieve info from the pool
            res = pool.value<T>(keyword);
        }
        return res;
    }


    template <typename T>
    static T extract_from_file(std::string filename, const char* keyword, bool low = true, const char * type ="json"){
        // get true filename
        if(low)
            filename = LL_DIR + filename + "." +  type;
        else
            filename = HL_DIR + filename + "." + type;

        // init pool
        essentia::Pool pool;
        essentia::standard::AlgorithmFactory &factory = essentia::standard::AlgorithmFactory::instance();
        essentia::standard::Algorithm *yamlInput = factory.create("YamlInput",
                                                                  "filename", filename,
                                                                  "format", type);
        yamlInput->output("pool").set(pool);
        yamlInput->compute();

        T res{};
        // check if the pool contains the keyword
        if(pool.contains<T>(keyword)){
            // retrieve info from the pool
            res = pool.value<T>(keyword);
        }

        return res;
    }
};


#endif //CREASOUND_EXTRACTOR_H
