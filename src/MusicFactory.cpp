#include "MusicFactory.h"
#include "Utils.h"
#include "Extractor.h"
#include "Sound.h"
#include "Learning.h"
#include "SoundTouchAPI.h"
#include <sys/stat.h>
#include <gaia.h>
#include <dataset.h>
#include <point.h>
#include <essentia/streaming/algorithms/poolstorage.h>
#include <essentia/scheduler/network.h>

using namespace std;
using namespace essentia;
using namespace standard;
using namespace essentia::streaming;
using namespace essentia::scheduler;
using namespace gaia2;


typedef vector<std::string> template_vect_string;

static Real sampleRate = 44100.0;
static int frameSize = 4096;
static int transitSize = frameSize/5;
static int hopSize = frameSize;

void fade(int previousFrame, int nextFrame, vector<Real> &previousFrameV, vector<Real> &nextFrameV, vector<Real> &tmp, int size1);


void compute_some_information(std::string filename1, std::string filename2);




bool MusicFactory::createMusic(string f1, string f2, string out, MusicFactoryAlgorithm algo, Config* conf) {
    Sound sound;
    int askSave = ASR_YES;
    
    // register the algorithms in the factory(ies)
    standard::AlgorithmFactory &factory =standard::AlgorithmFactory::instance();

    // init audio loaders
    standard::Algorithm *audioLoader1 = factory.create("EqloudLoader",
                                             "filename", f1,
                                             "sampleRate", sampleRate);
    standard::Algorithm *audioLoader2 = factory.create("EqloudLoader",
                                             "filename", f2,
                                             "sampleRate", sampleRate);

    // init frame creators
    standard::Algorithm *fc1 = factory.create("FrameCutter",
                                    "frameSize", frameSize,
                                    "hopSize", hopSize);
    standard::Algorithm *fc2 = factory.create("FrameCutter",
                                    "frameSize", frameSize,
                                    "hopSize", hopSize);

    // init input vectors
    vector<Real> audio_input_1, audio_input_2;

    // set audio loader to first input track
    audioLoader1->output("audio").set(audio_input_1);
    audioLoader1->compute();

    // set audio loader to second input track
    audioLoader2->output("audio").set(audio_input_2);
    audioLoader2->compute();

    fc1->input("signal").set(audio_input_1);
    fc2->input("signal").set(audio_input_2);

    vector<Real> result;

    Learning svm;
    svm.init();
    Pool infos;

    switch (algo) {
        case MF_ALGO_MERGE:
            result = merge(fc1, fc2);
            break;
        case MF_ALGO_SWITCH:
            result = switchShuffle(fc1, fc2);
            break;
        case MF_ALGO_RAND:
            result = randShuffle(fc1, fc2);
            break;
        case MF_CUSTOM:
            result = randWithFadeDetection(fc1, fc2, f1, f2);
            break;
        case MF_MULTIPLEXER:
            result = multiplexer(fc1, fc2);
            break;
        case MF_TONAL:
            result = switchWithTonalDetection(fc1, fc2, f1, f2);
            break;
        case MF_SVM:
            result = switchWithSVM(fc1, fc2, f1, f2);
            break;
        default:
            cerr << "Unknown algorithm" << endl;
            return false;
    }

    if (!result.size()){
        cerr << "Empty generation" << endl;
    }
    else
    {
        standard::Algorithm* audioWriter = factory.create("MonoWriter",
                                                          "filename", "generated/audio/tmp.wav",
                                                          "sampleRate", sampleRate);
        audioWriter->input("audio").set(result);
        audioWriter->compute();

        delete audioWriter;

        SoundTouchAPI::getInstance()->ChangeMusic("generated/audio/tmp.wav", "generated/audio/n_tmp.wav", conf->tempo, conf->pitch);
        
        result.clear();
        
        standard::Algorithm* audioLoader = factory.create("EqloudLoader",
                                             "filename", "generated/audio/n_tmp.wav",
                                             "sampleRate", sampleRate);
        
        audioLoader->output("audio").set(result);
        audioLoader->compute();
        
        delete audioLoader;
        
        remove("generated/audio/tmp.wav");
                                             
        audioWriter = factory.create("MonoWriter",
                    "filename", "generated/audio/tmp.wav",
                    "sampleRate", sampleRate);
                    
        audioWriter->input("audio").set(result);
        audioWriter->compute();
        
        delete audioWriter;
        
        sound.play("generated/audio/tmp.wav");
        
        std::string save;
        std::cout << "Do you want to save the music " << out << " (y/n) ?" << std::endl;
        std::cout << "Or type stop to stop the generation" << std::endl;
        
        while(sound.isPlaying())
        {
            std::cin >> save;
            std::transform(save.begin(), save.end(), save.begin(), ::tolower);
        
            if (save == "y")
                askSave =  ASR_YES;
            else if(save == "stop")
                askSave = ASR_STOP;
            else if(save == "n")
                askSave = ASR_NO;
            else
                continue;
                
            break;
        }
        if (askSave != ASR_NO && askSave != ASR_STOP){
            saveMusic(result, out, sampleRate);
            if(algo == MF_SVM) // user asked for svm
            {
                // get filename
                std::string name = Utils::extract_filename(out);

                // compute information on generated music
                Extractor::compute_lowlevel_information("generated/audio/tmp.wav" , "good_" + name);
                infos = Extractor::compute_highlevel_information("good_" + name, "good_" + name);

                // retrieve data from file
                std::string bpm = Extractor::extract_from_file<std::string>("good_" + name, "rhythm.bpm");
                std::string key = Extractor::extract_from_file<std::string>("good_" + name, "tonal.key_key");

                // store point
                svm.add_point_svm(true, out, svm.getDataSet(), bpm.c_str(), key.c_str());
            }
            std::string stop;
            std::cout << "Press stop to end the music" << std::endl;
            while(sound.isPlaying())
            {
                std::cin >> stop;
                if(stop == "stop")
                    sound.stop();
            }
            // stop playing beautiful music
            sound.stop();
        } else {
            // stop playing horrible music
            sound.stop();
            if(algo == MF_SVM) // user asked for svm
            {
                // get filename
                std::string name = Utils::extract_filename(out);

                // compute information on generated music
                Extractor::compute_lowlevel_information("generated/audio/tmp.wav", "bad_" + name);
                infos = Extractor::compute_highlevel_information("bad_" + name, "bad_" + name);

                // retrieve data from file
                std::string bpm = Extractor::extract_from_file<std::string>("bad_" + name, "rhythm.bpm");
                std::string key = Extractor::extract_from_file<std::string>("bad_" + name, "tonal.key_key");

                // store point
                svm.add_point_svm(true, out, svm.getDataSet(), bpm.c_str(), key.c_str());}
        }
    }

    if(algo == MF_SVM){
        svm.train();
/*        standard::Algorithm *gaia = factory.create("GaiaTransform" , "history", "highlevel.genre_tzanetakis");
        gaia->input("pool").set(infos);
        gaia->output("pool").set(infos);
        //gaia->configure();

        gaia->compute();*/

    }
    
    remove("generated/audio/tmp.wav");
    remove("generated/audio/n_tmp.wav");

    // clean variables
    delete fc1;
    delete fc2;
    delete audioLoader1;
    delete audioLoader2;
    
    return (askSave != ASR_STOP);
}


vector<Real> MusicFactory::switchShuffle(standard::Algorithm* fact1,  standard::Algorithm* fact2)
{

    int state = 0;
    vector<Real> output, frames1, frames2, tmp(frameSize);

    fact1->output("frame").set(frames1);
    fact2->output("frame").set(frames2);

    while (true) {
        // compute a frame
        fact1->compute();
        fact2->compute();

        if (!frames1.size() || !frames2.size())
            break;

        // if frames are silent, just drop it and go on processing
        //if (isSilent(frames1) && isSilent(frames2)) continue;

        int size1 = min(frames1.size(), frames2.size());
        tmp.clear();

        switch (state) {
            case 0:
                for (int i = 0; i < size1; i++) {
                    float t = max(((float) (transitSize - i) / (float) transitSize), 0.2f);
                    tmp[i] = (frames2[i] * t) + (frames1[i] * (1 - t));
                }
                break;
            case 2:
                for (int i = 0; i < size1; i++) {
                    float t = max(((float) (transitSize - i) / (float) transitSize), 0.2f);
                    tmp[i] = (frames1[i] * t) + (frames2[i] * (1 - t));
                }
                break;
            case 1:
            case 3:
                for (int i = 0; i < size1; i++)
                    tmp[i] = frames2[i] / 2 + frames1[i] / 2;
                break;
        }
        state = (state + 1) % 4;

        for (int i = 0; i < size1; i++)
            output.push_back(tmp[i]);
    }

    return output;
}


vector<Real> MusicFactory::merge(standard::Algorithm* fact1,  standard::Algorithm* fact2)
{
    vector<Real> output, frames1, frames2, tmp;
    tmp.resize(frameSize);

    fact1->output("frame").set(frames1);
    fact2->output("frame").set(frames2);

    while (true) {
        // compute a frame
        fact1->compute();
        fact2->compute();

        if (!frames1.size() || !frames2.size())
            break;
        int size1 = min(frames1.size(), frames2.size());
        for (int i = 0; i < size1; i++)
            output.push_back(frames1[i] + frames2[i]);
    }

    return output;
}

vector<Real> MusicFactory::multiplexer(standard::Algorithm* fact1,  standard::Algorithm* fact2)
{
    vector<Real> output, frame1, frame2, tmp;
    vector<vector<Real> > frame_vector, f1, f2;
    
    tmp.resize(frameSize);

    fact1->output("frame").set(frame1);
    fact2->output("frame").set(frame2);
    
    standard::AlgorithmFactory &factory =standard::AlgorithmFactory::instance();
    
    standard::Algorithm *multiplexer = factory.create("Multiplexer",
                                                      "numberVectorRealInputs", 2);
                                             
    multiplexer->output("data").set(frame_vector);
    multiplexer->input("vector_0").set(f1);
    multiplexer->input("vector_1").set(f2);
                                                  
    while (true) {
        // compute a frame
        fact1->compute();
        fact2->compute();

        if (!frame1.size() || !frame2.size())
            break;
        
        f1.clear();
        f2.clear();
        f1.push_back(frame1);
        f2.push_back(frame2);
        f1.push_back(frame1);
        f2.push_back(frame2);
        
        frame_vector.clear();
        frame_vector.push_back(frame1);
        frame_vector.push_back(frame2);
        
        multiplexer->compute();
        
        tmp = frame_vector[0];
                
        for (size_t i = 0; i < tmp.size(); i++)
            output.push_back(tmp[i]);
    }
    return output;
}



vector<Real> MusicFactory::randShuffle(standard::Algorithm *fact1, standard::Algorithm *fact2) {
    int previousFrameID;
    vector<Real> output, frames1, frames2, tmp, previousFrameV;

    tmp.resize(frameSize);
    bool once = true;

    fact1->output("frame").set(frames1);
    fact2->output("frame").set(frames2);

    while (true) {
        // compute a frame
        fact1->compute();
        fact2->compute();

        if (once) { // must be done after the compute
            once = false;
            // default set of the previous frame
            previousFrameV = frames1;
            previousFrameID = 1;
        }


        if (!frames1.size() || !frames2.size())
            break;

        // if frames are silent, just drop it and go on processing
        //  if (isSilent(frames1) && isSilent(frames2)) continue; useful ?

        int size1 = min(frames1.size(), frames2.size());
        tmp.clear();
        switch (rand() % 4) {
            case 0: // case frames1
                //for (int i = 0; i < size1; i++)
                //    tmp.push_back(frames1[i]);
                fade(previousFrameID, 1, previousFrameV, frames1, tmp, size1);
                break;
            case 1: // case frames2
                //for (int i = 0; i < size1; i++)
                //    tmp.push_back(frames2[i]);
                fade(previousFrameID, 2, previousFrameV, frames2, tmp, size1);
                break;
            case 2:
            case 3:
                for (int i = 0; i < size1; i++) {
                    tmp.push_back(frames2[i] / 2 + frames1[i] / 2);
                }
                break;
        }
        // if it was the last one (ie: it was empty), then we're done.
        if (!tmp.size()) {
            break;
        }

        for (int i = 0; i < size1; i++)
            output.push_back(tmp[i]);
    }
    return output;
}

void fade(int previousFrameID, int nextFrameID, vector<Real> &previousFrameV, vector<Real> &nextFrameV, vector<Real> &tmp, int size1) {
    float t; // transition factor between two frames
    float threshold = 0.2f;
    for (int i = 0; i < size1; i++) {
        if (previousFrameID == nextFrameID) { // no change if the previous frame is the same that the current
            tmp.push_back(nextFrameV[i]);
        } else {
            // float needed in order to take the float value rather than the integer part
            t = 1.f - (float) (i % (size1 / 2)) / (size1 / 2); // \_/
            if (i < size1 / 2)
                tmp.push_back(previousFrameV[i] * max(t,threshold));
            else
                tmp.push_back(nextFrameV[i] * max((1 - t),threshold));
        }
    }
    previousFrameID = nextFrameID;
    previousFrameV = nextFrameV;
}

void MusicFactory::saveMusic(vector<Real> res, string out, Real sampleRate) {
    // Choose directory
    string path;
    /*cout << "Where do you want to save the music ? ";
    cin >> path;  */

    path = "generated/audio/";
    _mkdir(path.c_str());
    char cwd[PATH_MAX];
    char* result = realpath(path.c_str(), cwd); 
    if(result != cwd)
    {
        std::cerr << "Incorrect path: '" << path << "'\nThe music will be saved in the current directory" << std::endl;
        path = "./";
    }
    else
        path = cwd;
    
    if(path[path.length()-1] != '/')
        path += '/';
    
    out = path + out;    

    standard::AlgorithmFactory &factory = standard::AlgorithmFactory::instance();
    // init audio writer
    standard::Algorithm *audioWriter = factory.create("MonoWriter",
                                            "filename", out,
                                            "sampleRate", sampleRate);

    // Write the output file
    audioWriter->input("audio").set(res);
    audioWriter->compute();

    delete audioWriter;
}


void MusicFactory::_mkdir(const char *dir) {
        char tmp[256];
        char *p = NULL;
        size_t len;

        snprintf(tmp, sizeof(tmp),"%s",dir);
        len = strlen(tmp);
        if(tmp[len - 1] == '/')
                tmp[len - 1] = 0;
        for(p = tmp + 1; *p; p++)
                if(*p == '/') {
                        *p = 0;
                        mkdir(tmp, S_IRWXU);
                        *p = '/';
                }
        mkdir(tmp, S_IRWXU);
}

std::vector<essentia::Real> MusicFactory::randWithFadeDetection(essentia::standard::Algorithm *fact1,
                                                         essentia::standard::Algorithm *fact2,
                                                         std::string filename1,
                                                         std::string filename2 ) {
    vector<Real> output, frames1, frames2, tmp1, tmp2;

    vector<vector<Real>> fades_1, fades_2;
    fades_1 = Extractor::compute_phrases_from_fades(filename1);
    fades_2 = Extractor::compute_phrases_from_fades(filename2);

    cout << "fades 1 size : " << fades_1.size() << endl;
    cout << "fades 2 size : " << fades_2.size() << endl;

    vector<vector<Real>> phrases;
    
    // compute_some_information(filename1, filename2);

    fact1->output("frame").set(frames1);
    fact2->output("frame").set(frames2);

    size_t current_pos_1 = 0, current_pos_2 = 0;
    size_t fade_index_1 = 0, fade_index_2 = 0;
    size_t index_1 =0 , index_2 = 0;
    bool insere_1 = false, insere_2 = false;

    while (true) {
        // compute a frame
        fact1->compute();
        fact2->compute();

        // compute new position
        current_pos_1 += frames1.size();
        current_pos_2 += frames2.size();

        // if end, stop
        if(!frames1.size() || !frames2.size())
            break;

        // save current frame 1 ?
        if(fade_index_1 < fades_1.size()
           && (Utils::sec_to_frame(fades_1[fade_index_1][IN]) + frames1.size()) <= current_pos_1){
            // set a flag
            insere_1 = true;
        }

        // end of current frame 1 ?
        if(fade_index_1 < fades_1.size()
           && Utils::sec_to_frame(fades_1[fade_index_1][OUT]) < current_pos_1
           && insere_1){ // tu peux enregistrer la frame1 courante

            // set a flag
            insere_1 = false;
            fade_index_1++;
            phrases.push_back(tmp1);
            tmp1.clear();
        }

        // save current frame 2 ?
        if(fade_index_2 < fades_2.size()
           && Utils::sec_to_frame(fades_2[fade_index_2][IN]) + frames2.size() <= current_pos_2){ // tu peux enregistrer la frame1 courante
            // set a flag
            insere_2 = true;
        }

        // end of current frame 2 ?
        if(fade_index_2 < fades_2.size()
           && Utils::sec_to_frame(fades_2[fade_index_2][OUT]) < current_pos_2
           && insere_2){ // tu peux enregistrer la frame1 courante
            // set a flag
            insere_2 = false;
            fade_index_2++;
            phrases.push_back(tmp2);
            tmp2.clear();
        }

        // save values from frame 1
        if(insere_1){
            index_1 = fades_1[fade_index_1][IN];
            for(size_t j = index_1; j < fades_1[fade_index_1][OUT] || j < frames1.size(); j++){
                tmp1.push_back(frames1[j]);
            }
        }

        // save values from frame 2
        if(insere_2){
            index_2 = fades_2[fade_index_2][IN];
            for(size_t j = index_2; j < fades_2[fade_index_2][OUT] || j < frames2.size(); j++){
                tmp2.push_back(frames2[j]);
            }
        }

    }

    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    std::uniform_int_distribution<int> uni(0, phrases.size()-1); // guaranteed unbiased

    int sizeMusic = phrases.size() / 2;
    while(sizeMusic > 0)
    {
        auto index = uni(rng);
        for (size_t k = 0; k < phrases[index].size(); k++) // all frames
        { 
            output.push_back(phrases[index][k]); // copying to result
        }
        sizeMusic--;
    }

    return output;
}


vector<Real> MusicFactory::switchWithTonalDetection( essentia::standard::Algorithm* fact1,
                                                     essentia::standard::Algorithm* fact2,
                                                     std::string filename1,
                                                     std::string filename2 )
{
    vector<Real> output, frames1, frames2, tmp;

    // count the number of phrases
    int sizeMusic;
    // store phrases
    vector<vector<Real>> phrases;

    Pool p1 = Extractor::compute_tonal_information(filename1);
    Pool p2 = Extractor::compute_tonal_information(filename2);

    template_vect_string tonals_1 = Extractor::extract_from_pool<template_vect_string>(p1, "tonal.chords_progression");
    template_vect_string tonals_2 = Extractor::extract_from_pool<template_vect_string>(p2, "tonal.chords_progression");

    size_t cpt = 1;
    string current_key = "";
    // repetitions of tonals, (["A","A","A","A","B","B","A"] will be [4,2,1] for exemple)
    vector<int> tonal_repetitions1, tonal_repetitions2;
    if(!tonals_1.empty()){ // non-empty tonals
        for(size_t i = 0 ; i < tonals_1.size() ; i++){ // fetching all tonals
            if(current_key == tonals_1.at(i)){ // previous and current tonals are equals (["A","A"])
                cpt++;
            } else { // previous and current tonals are different (["A","B"])
                current_key = tonals_1.at(i);
                tonal_repetitions1.push_back(cpt); // adding number of occurences of the current tonal
                // increment phrases counter
                sizeMusic += cpt;
                cpt = 1; // first tonal found
            }
        }
    }

    size_t cpt2 = 1;
    current_key = "";
    if(!tonals_2.empty()){ // non-empty tonals
        for(size_t j = 0 ; j < tonals_2.size() ; j++){ // fetching all tonals
            if(current_key == tonals_2.at(j)){ // previous and current tonals are equals (["A","A"])
                cpt2++;
            } else { // previous and current tonals are different (["A","B"])
                current_key = tonals_2.at(j);
                tonal_repetitions2.push_back(cpt2); // adding number of occurences of the current tonal
                // increment phrases counter
                sizeMusic += cpt2;
                cpt2 = 1; // first tonal found
            }
        }
    }

    // mean of phrases counter
    sizeMusic /= 2;

    fact1->output("frame").set(frames1);
    fact2->output("frame").set(frames2);

    cpt = 0; cpt2 = 0;
    // size_t transitTime1 = transitSize, transitTime2 = transitSize;

    cout << "(1) :" << tonals_1.size() << endl;
    cout << "(2) :" << tonals_2.size() << endl;
    
    size_t size1, size2;

    while (true) { // while data to process

        // compute a frame
        fact1->compute();
        fact2->compute();

        size1 = frames2.size();
        size2 = frames1.size();
        if (!size1 || !size2)
            break;

        phrases.push_back(frames1);
        phrases.push_back(frames2);

        tmp.clear();
        tmp.resize(size1+size2);

        cpt++; cpt2++;

        // set new configuration
        if(cpt2 < tonal_repetitions2.size()){ // if fact2 needs to be reconfigured
            fact2->configure("frameSize", tonal_repetitions2.at(cpt2) * frameSize);
            //transitTime2 = tonal_repetitions2.at(cpt2) * frameSize / 5;
        }
        if(cpt < tonal_repetitions1.size()){ // if fact1 needs to be reconfigured
            fact1->configure("frameSize", tonal_repetitions1.at(cpt) * frameSize);
            //transitTime1 = tonal_repetitions1.at(cpt) * frameSize / 5;
        }
    }
    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    std::uniform_int_distribution<int> uni(0,phrases.size()); // guaranteed unbiased

    sizeMusic = phrases.size() / 2;
    while(sizeMusic > 0){

        auto index = uni(rng);
        for (size_t k = 0; k < phrases[index].size(); k++){ // all frames
            output.push_back(phrases[index][k]); // copying to result
        }
        sizeMusic--;// -= phrases[index].size();
    }



    return output;
}


vector<Real> MusicFactory::switchWithSVM( essentia::standard::Algorithm* fact1,
                                                          essentia::standard::Algorithm* fact2,
                                                          std::string filename1,
                                                          std::string filename2 )
{

    int state = 0;
    vector<Real> output, frames1, frames2, tmp(frameSize);

    std::string audio_name1 = Utils::extract_filename(filename1);
    std::string audio_name2 = Utils::extract_filename(filename2);

    Extractor::compute_lowlevel_information(filename1,
                                            audio_name1,
                                            "profiles/all_config.yaml");
    Extractor::compute_lowlevel_information(filename2,
                                            audio_name2,
                                            "profiles/all_config.yaml");
    // ?
    //Learning::addSVMDescriptors();


    Pool info1 = Extractor::compute_highlevel_information(audio_name1, audio_name1, "profiles/all_config.yaml");
    Pool info2 = Extractor::compute_highlevel_information(audio_name2, audio_name2, "profiles/all_config.yaml");



    fact1->output("frame").set(frames1);
    fact2->output("frame").set(frames2);

    while (true) {
        // compute a frame
        fact1->compute();
        fact2->compute();

        if (!frames1.size() || !frames2.size())
            break;

        // if frames are silent, just drop it and go on processing
        //if (isSilent(frames1) && isSilent(frames2)) continue;

        int size1 = min(frames1.size(), frames2.size());
        tmp.clear();

        switch (state) {
            case 0:
                for (int i = 0; i < size1; i++) {
                    float t = max(((float) (transitSize - i) / (float) transitSize), 0.2f);
                    tmp[i] = (frames2[i] * t) + (frames1[i] * (1 - t));
                }
                break;
            case 2:
                for (int i = 0; i < size1; i++) {
                    float t = max(((float) (transitSize - i) / (float) transitSize), 0.2f);
                    tmp[i] = (frames1[i] * t) + (frames2[i] * (1 - t));
                }
                break;
            case 1:
            case 3:
                for (int i = 0; i < size1; i++)
                    tmp[i] = frames2[i] / 2 + frames1[i] / 2;
                break;
        }
        state = (state + 1) % 4;

        for (int i = 0; i < size1; i++)
            output.push_back(tmp[i]);
    }

    return output;
}



void compute_some_information(std::string filename1, std::string filename2){
    Pool info1 = Extractor::compute_tonal_information(filename1);
    Pool info2 = Extractor::compute_tonal_information(filename2);
    Pool info3 = Extractor::compute_rythm_information(filename1);
    Pool info4 = Extractor::compute_rythm_information(filename2);

    const string key_keyword = "tonal.keys";
    string key_1, key_2;
    if(info1.contains<string>(key_keyword))
        key_1 = info1.value<string>(key_keyword);
    if(info2.contains<string>(key_keyword))
        key_2 = info2.value<string>(key_keyword);

    const string bpm_keyword = "rhythm.bpm";
    Real bpm_1, bpm_2;
    if(info3.contains<Real>(bpm_keyword))
        bpm_1 = info3.value<Real>(bpm_keyword);
    if(info4.contains<Real>(bpm_keyword))
        bpm_2 = info4.value<Real>(bpm_keyword);

    cout << "Keys (1) : " << key_1 << ", (2) : " << key_2 << endl;
    cout << "BPMs (1) : " << bpm_1 << ", (2) : " << bpm_2 << endl;
}
