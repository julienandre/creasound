#include <linux/limits.h>
#include "Utils.h"
#include "Extractor.h"
#include "SoundTouchAPI.h"
#include <string.h>
#include <unistd.h>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <essentia/algorithmfactory.h>
#include <essentia/pool.h>
#include <algorithm>

using namespace essentia;

char Utils::separator() {
#ifdef _WIN32
    return '\\';
#else
    return '/';
#endif
}

bool Utils::is_valid_music (const std::string& name) {
    // check whether if file exists
    if(access(name.c_str(), F_OK ) == -1)
    {
        std::cerr << "ERROR: invalid argument." << std::endl;
        std::cerr << "Usage: " << name << " should be a path to an audio file" << std::endl;
        return false;
    } 
    else 
    {
        // check file extension (accepted : mp3, wav)
        if(Utils::good_music_extension(name))
        {
            std::cerr << name << " is a valid audio file" << std::endl;
            return true;
        }
        else
        {
            std::cerr << "ERROR: invalid argument." << std::endl;
            std::cerr << "Usage: " << name << " iis not a valid audio file (.mp3, .wav)" << std::endl;
            return false;
        }
    }
}


bool Utils::good_music_extension(const std::string &name) {
    std::string extension = name.substr(name.find_last_of(".") + 1);
    return (extension == "mp3" || extension=="wav");
}

int Utils::ask_save(std::string filename)
{
    std::string save;
    std::cout << "Do you want to save the music " << filename << " (y/n) ?" << std::endl;
    std::cout << "Or type stop to stop the generation" << std::endl;
    std::cin >> save;
    std::transform(save.begin(), save.end(), save.begin(), ::tolower);
    if (save == "y")
        return ASR_YES;
    else if(save == "stop")
        return ASR_STOP;
    else
        return ASR_NO;
}

void Utils::process_directory( const std::string &dirName, int (*fct)(const std::string&)) {
    char rp[PATH_MAX];
    DIR *dirp = opendir(dirName.c_str());
    if (dirp) {
        struct dirent *dp = NULL;
        while ((dp = readdir(dirp)) != NULL) {
            std::string file(dp->d_name);
            if (file == "." || file == "..") // skip current dir and parent dir
                continue;
            if (dp->d_type & DT_DIR) {
                // dir: call function inside it
                std::string filePath = dirName + "/" + file;
                process_directory(filePath, fct);
            }
            else {
                // found a file
                char* path = realpath((dirName + separator() + file).c_str(), rp);
                if(path != rp)
                    continue;
                    
                fct(std::string(rp));
            }
        }
        closedir(dirp);
    }
}

bool Utils::file_exist (const std::string& name) {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}

size_t Utils::sec_to_frame(const float sec, const int sampleRate){
    return sec * sampleRate;
}

void Utils::changeTempoValue(std::string in, std::string out, int value) {
    Pool p;
    standard::Algorithm *input = standard::AlgorithmFactory::create("YamlInput",
                                                                    "filename", in + ".rhythm.yaml");
    input->output("pool").set(p);
    input->compute();
    Real bpm = Extractor::extract_from_pool<Real>(p, "rhythm.bpm");
    int bpmChange = (value * 100 / bpm) - 100;
    SoundTouchAPI::getInstance()->ChangeTempo(in, out, bpmChange);
}

std::string Utils::extract_filename(std::string path)
{
    int pos = path.find_last_of("/")+1;
    return path.substr(pos, path.find_last_of(".") - pos);

}