#ifndef CREASOUND_SOUND_H
#define CREASOUND_SOUND_H

#include <SFML/Audio.hpp>

using namespace std;

class Sound {
private:
    sf::SoundBuffer buffer;
    sf::Sound sound;

public:
    int load(string file){
        if (!buffer.loadFromFile(file))
            return -1;
        sound.setBuffer(buffer);
        return 0;
    }

    int play(string file){
        int err = Sound::load(file);
        if(err < 0)
            return err;
        sound.play();
        return 0;
    }

    void stop(){
        sound.stop();
    }

    bool isPlaying(){
        return sound.getStatus() == sf::SoundSource::Status::Playing;
    }
};

#endif //CREASOUND_SOUND_H
