#include "Config.h"
#include "PlaylistManager.h"

#include "dirent.h"
#include <fstream>

using namespace std;

bool Config::save(string file)
{
    ofstream out(file);
    out << tempo << endl;
    out << pitch << endl;
    out << generationMode << endl;
    out << (playlistName == "" ? "#"  : playlistName) << endl;
    for(string str : tracks)
        out << str << endl;
        
    out.close();
    
    return true;
}


Config* Config::load(string file)
{
    ifstream in(file);
    
    Config* conf = new Config();

    string name = file;
    int start = name.find_last_of("/")+1;
    name = name.substr(start, name.find_last_of(".")-start);
    conf->configName = name;
    
    if(in.is_open())
    {
        conf->tracks.clear();
        
        in >> conf->tempo;
        in >> conf->pitch;
        in >> conf->generationMode;
        in >> conf->playlistName;
        
        if(conf->outName == "#")
            conf->outName = "default.mp3";
        
        conf->_playlist = (conf->playlistName != "#") 
                         ? PlaylistManager::getInstance()->getPlaylist(conf->playlistName)
                         : nullptr
                         ;
        
        string track;
        
        while(!in.eof())
        {
            in >> track;
            //conf->tracks.insert(track);
        }       
        
        in.close();
        
        return conf;
    }
    else
        return nullptr;
}

vector<string> Config::listConfig()
{
    vector<string> configs;
    
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir ("config/")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            std::string name(ent->d_name);
            if(name == "" || name == "." || name == "..")
                continue;
            int start = name.find_last_of("/")+1;
            name = name.substr(start, name.find_last_of(".")-start);
            configs.push_back(name);
        }
        closedir(dir);
    }
    
    return configs;
}