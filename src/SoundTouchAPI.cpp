#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <sstream>

#include "SoundTouchAPI.h"
#include "SoundTouch.h"
#include "Utils.h"
#include "./soundstretch/WavFile.h"

using namespace soundtouch;
using namespace std;


bool SoundTouchAPI::ChangeTempo(string in, string out, float tempoChange)
{
    return ChangeMusic(in, out, tempoChange);
}

bool SoundTouchAPI::ChangePitch(string in, string out, float semiton)
{    
    return ChangeMusic(in, out, 0.0f, semiton);
}  


bool SoundTouchAPI::ChangeMusic(string in, string out, float tempoChange, float semiton)
{
    if(!CheckFiles(in, out))
        return false;
    
    if(tempoChange < -95.0f || tempoChange > 5000.0f)
    {
        cerr << "-95.0 <= tempoChange <= 5000.0" << endl;
        return false;
    }
    
    if(abs(semiton) > 60.0f)
    {
        cerr << "-60.0 <= semiton <= 60.0" << endl;
        return false;
    }
        
    WavInFile* wavIn = new WavInFile(in.c_str());
    SoundTouch soundTouch;
    
    int sampleRate;
    int nChannels;
    int nSamples;
    int buffSizeSamples;

    sampleRate = (int)wavIn->getSampleRate();
    nChannels = (int)wavIn->getNumChannels();
    soundTouch.setSampleRate(sampleRate);
    soundTouch.setChannels(nChannels);
    soundTouch.setTempoChange(tempoChange);
    soundTouch.setPitchSemiTones(semiton);
    
    SAMPLETYPE sampleBuffer[BUFF_SIZE];
    
    assert(nChannels > 0);
    buffSizeSamples = BUFF_SIZE / nChannels;
    
    WavOutFile* wavOut = new WavOutFile(out.c_str(), sampleRate, (int)wavIn->getNumBits(), nChannels);

    // Process samples read from the input file
    while (wavIn->eof() == 0)
    {
        int num;

        // Read a chunk of samples from the input file
        num = wavIn->read(sampleBuffer, BUFF_SIZE);
        nSamples = num / (int)wavIn->getNumChannels();

        // Feed the samples into SoundTouch processor
        soundTouch.putSamples(sampleBuffer, nSamples);

        do 
        {
            nSamples = soundTouch.receiveSamples(sampleBuffer, buffSizeSamples);
            wavOut->write(sampleBuffer, nSamples * nChannels);
        } while (nSamples != 0);
    }

    // Now the input file is processed, yet 'flush' few last samples that are
    // hiding in the SoundTouch's internal processing pipeline.
    soundTouch.flush();
    do 
    {
        nSamples = soundTouch.receiveSamples(sampleBuffer, buffSizeSamples);
        wavOut->write(sampleBuffer, nSamples * nChannels);
    } while (nSamples != 0);
    
    return true;
}


bool SoundTouchAPI::CheckFiles(string in, string out)
{
    if(in == out)
    {
        cerr << "CheckFiles: Input and Output files must be different" << endl;
        return false;
    }
    
    return Utils::good_music_extension(out) && Utils::is_valid_music(in);
}