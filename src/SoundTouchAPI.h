#ifndef SOUND_TOUCH_API_H
#define SOUND_TOUCH_API_H

#include <string>

#define BUFF_SIZE 6720

class SoundTouchAPI
{
public:
    static SoundTouchAPI* getInstance()
    {
        static SoundTouchAPI instance;
        return &instance;
    }
    
    bool ChangeTempo(std::string in, std::string out, float tempoChange);
    bool ChangePitch(std::string in, std::string out, float semiton);
    
    bool ChangeMusic(std::string in, std::string out, float tempoChange = 0.0f, float semiton = 0.0f);

private:
    SoundTouchAPI(){}
    bool CheckFiles(std::string in, std::string out);    
    
};

#endif



