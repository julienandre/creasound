//
// Created by user on 31/05/16.
//

#include "Playlist.h"
#include <cstdlib>
#include <fstream>
#include <linux/limits.h>

bool Playlist::save() {
    std::ofstream file;

    // open playlist file
    std::string filename("generated/playlists/" + _name + ".txt");
    file.open(filename.c_str(), std::ios_base::out);

    // check if the file is open
    if(!file.is_open()){
        std::cout << "ERROR : Could not open file " << filename << std::endl;
        return false;
    }

    for(auto track : _tracks)
    {
        file << track << std::endl;
    }

    // close the file
    file.close();
    
    return true;
}


bool Playlist::load() {
    _tracks.clear();
    
    std::ifstream file;

    std::string path = "generated/playlists/" + _name + ".txt";
    // open playlist file
    file.open(path.c_str());

    // check if the file is open
    if(file.is_open()){
        std::string line;
        while(std::getline(file, line)){
            _tracks.insert(line);
        }        
        file.close();
    }
    //else
        //std::cout << "Could not open file " << path << std::endl;
    
    return !_tracks.empty();
}

bool Playlist::addTrack(const std::string &path) {
    char cwd[PATH_MAX];
    char* result = realpath(path.c_str(), cwd);
    if(result != cwd)
    {
        std::cerr << "Incorrect path: '" << path << "'" << std::endl;
        return false;
    }
    
    changed = true;
    _tracks.insert(cwd);
    return true;
}

bool Playlist::removeTrack(const std::string &path) {
    if(_tracks.find(path) == _tracks.end())
        return false;
    
    changed = true;    
    _tracks.erase(path);
    return true;
}

bool Playlist::removeTrack(const std::size_t index)
{
    if(index > _tracks.size())
        return false;
    
    changed = true;
    _tracks.erase(*std::next(_tracks.begin(), index));
    return true;
}

void Playlist::print() const
{
    std::cout << "Content: " << std::endl;
    for (std::string elem : _tracks) {
        elem = elem.substr(elem.find_last_of("/") + 1);
        std::cout << "- " << elem << std::endl;
    }
}

std::set<std::string> Playlist::getRandomTracks(size_t count)
{
    std::set<std::string> randomTracks;
    size_t safety = 25, tc = _tracks.size();
    
    do
    {
        randomTracks.insert(*std::next(_tracks.begin(), rand()%tc));
    }while(randomTracks.size() != count && safety--);
    
    return randomTracks;
}