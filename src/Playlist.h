#ifndef CREASOUND_PLAYLIST_H
#define CREASOUND_PLAYLIST_H

#include <iostream>
#include <set>
#include "Utils.h"


class Playlist {
private:
    std::set<std::string> _tracks;
    std::string _name;

public:
    bool changed = false;

    // constructors
    Playlist(std::string name) : _name(name) { load(); }
   
    // others methods
    bool save();
    bool load();
    void rename(std::string name){ 
        _name = name;
        changed = true; 
    }
    
    bool addTrack(const std::string& path);
    bool removeTrack(const std::string& path);
    bool removeTrack(const std::size_t index);
    std::set<std::string> getRandomTracks(size_t count = 2);
    void print() const;
    int trackCount() const { return _tracks.size(); }
    std::string name() const { return _name; }
};


#endif //CREASOUND_PLAYLIST_H
