//
// Created by user on 02/06/16.
//

#include "Extractor.h"
#include "Utils.h"
#include "extractor_music/MusicExtractor.h"
#include <stdlib.h>

/******/
#include <essentia/streaming/algorithms/poolstorage.h>
#include <essentia/utils/tnt/tnt2vector.h>
#include <essentia/scheduler/network.h>
#include "essentia/algorithmfactory.h"
#include <essentia/essentiautil.h>
#include "essentia/algorithm.h"
#include "essentia/pool.h"
#include <gaia2/gaia.h>
/******/

using namespace std;
using namespace essentia;
using namespace essentia::streaming;
using namespace essentia::scheduler;


/****************************/

typedef TNT::Array2D<Real> array2d;
const std::string Extractor::LL_DIR = "generated/descriptors/lowlevel/" ;
const std::string Extractor::HL_DIR = "generated/descriptors/highlevel/";
const std::string Extractor::PROFILE_DIR = "profiles/";


void AddToPool(const array2d &a2d,
               const string &desc, // descriptor names
               Pool &pool) {
    vector<vector<Real> > v2d = array2DToVecvec(a2d);
    for (size_t i = 0; i < v2d.size(); ++i)
        pool.add(desc, v2d[i]);
}

/****************************/




essentia::Pool Extractor::compute_tonal_information(std::string name) {

    // init pool result
    Pool pool;

    /////// PARAMS //////////////
    Real sampleRate = 44100.0;
    int frameSize = 8192;

    // we want to compute the MFCC of a file: we need the create the following:
    // audioloader -> framecutter -> windowing -> FFT -> MFCC -> PoolStorage
    // we also need a DevNull which is able to gobble data without doing anything
    // with it (required otherwise a buffer would be filled and blocking)

    streaming::AlgorithmFactory &factory = streaming::AlgorithmFactory::instance();

    streaming::Algorithm *audio = factory.create("MonoLoader",
                                                 "filename", name,
                                                 "sampleRate", sampleRate);

    streaming::Algorithm *tonal_ex = factory.create("TonalExtractor", "frameSize", frameSize);


    /////////// CONNECTING THE ALGORITHMS ////////////////
    cout << "-------- connecting algos (Tonal) --------" << endl;

    // audio -> tonal
    audio->output("audio") >> tonal_ex->input("signal");

    // link outputs
    tonal_ex->output("key_key")             >> PC(pool, "tonal.keys");
    tonal_ex->output("chords_changes_rate") >> PC(pool, "tonal.chords_changes_rate");
    tonal_ex->output("chords_histogram")    >> PC(pool, "tonal.chords_histogram");
    tonal_ex->output("chords_key")          >> PC(pool, "tonal.chords_key");
    tonal_ex->output("chords_number_rate")  >> PC(pool, "tonal.chords_number_rate");
    tonal_ex->output("chords_progression")  >> PC(pool, "tonal.chords_progression");
    tonal_ex->output("chords_scale")        >> PC(pool, "tonal.chords_scale");
    tonal_ex->output("chords_strength")     >> PC(pool, "tonal.chords_strength");
    tonal_ex->output("hpcp")                >> PC(pool, "tonal.hpcp");
    tonal_ex->output("hpcp_highres")        >> PC(pool, "tonal.hpcp_highres");
    tonal_ex->output("key_scale")           >> PC(pool, "tonal.key_scale");
    tonal_ex->output("key_strength")        >> PC(pool, "tonal.key_strength");

    //std::string key;
    //tonal_ex->output("key_key") >> key;
    // Note: PC is a #define for PoolConnector


    /////////// STARTING THE ALGORITHMS //////////////////
    cout << "-------- start retrieving tonal information on : " << name << " --------" << endl;

    // create a network with our algorithms...
    Network n(audio);
    // ...and run it, easy as that!
    n.run();

    // aggregate the results
    Pool aggrPool; // the pool with the aggregated MFCC values
    const char *stats[] = {"mean", "var", "min", "max", "cov"};

    standard::Algorithm *aggr = standard::AlgorithmFactory::create("PoolAggregator",
                                                                   "defaultStats", arrayToVector<string>(stats));

    aggr->input("input").set(pool);
    aggr->output("output").set(aggrPool);
    aggr->compute();


    string output_name(HL_DIR);
    output_name.append(Utils::extract_filename(name));
    output_name.append("_tonal.yaml");
    cout << "OUTNAME: " << output_name << endl;

    // write results to file
    cout << "-------- writing tonal results to file " << output_name << " --------" << endl;

    standard::Algorithm *output = standard::AlgorithmFactory::create("YamlOutput",
                                                                     "filename", output_name);
    output->input("pool").set(aggrPool);
    output->compute();


    // NB: we could just wait for the network to go out of scope, but then this would happen
    //     after the call to essentia::shutdown() where the FFTW structs would already have
    //     been freed, so let's just delete everything explicitly now
    n.clear();

    delete output;


    return pool;
}

essentia::Pool Extractor::compute_rythm_information(std::string name) {

    // init pool result
    Pool pool;

    /////// PARAMS //////////////
    Real sampleRate = 44100.0;
    int frameSize = 8192;

    // we want to compute the MFCC of a file: we need the create the following:
    // audioloader -> framecutter -> windowing -> FFT -> MFCC -> PoolStorage
    // we also need a DevNull which is able to gobble data without doing anything
    // with it (required otherwise a buffer would be filled and blocking)

    streaming::AlgorithmFactory &factory = streaming::AlgorithmFactory::instance();

    streaming::Algorithm *audio = factory.create("MonoLoader",
                                                 "filename", name,
                                                 "sampleRate", sampleRate);

    streaming::Algorithm *rythm_ex = factory.create("RhythmExtractor", "frameSize", frameSize);


    /////////// CONNECTING THE ALGORITHMS ////////////////
    cout << "-------- connecting algos (Rythm) --------" << endl;

    // audio -> tonal
    audio->output("audio")              >> rythm_ex->input("signal");

    // link outputs
    rythm_ex->output("bpm")             >> PC(pool, "rhythm.bpm");
    rythm_ex->output("ticks")           >> PC(pool, "rhythm.ticks");
    rythm_ex->output("estimates")       >> PC(pool, "rhythm.estimates");
    rythm_ex->output("bpmIntervals")    >> PC(pool, "rhythm.bpmIntervals");


    /////////// STARTING THE ALGORITHMS //////////////////
    cout << "-------- start retrieving rythm information on : " << name << " --------" << endl;

    // create a network with our algorithms...
    Network n(audio);
    // ...and run it, easy as that!
    n.run();

    // aggregate the results
    Pool aggrPool; // the pool with the aggregated MFCC values
    const char *stats[] = {"mean", "var", "min", "max", "cov"};

    standard::Algorithm *aggr = standard::AlgorithmFactory::create("PoolAggregator",
                                                                   "defaultStats", arrayToVector<string>(stats));

    aggr->input("input").set(pool);
    aggr->output("output").set(aggrPool);
    aggr->compute();


    string output_name(HL_DIR);
    output_name.append(Utils::extract_filename(name));
    output_name.append("_rhythm.yaml");

    // write results to file
    cout << "-------- writing rythm results to file " << output_name << " --------" << endl;

    standard::Algorithm *output = standard::AlgorithmFactory::create("YamlOutput",
                                                                     "filename", output_name);
    output->output("pool").set(aggrPool);
    output->compute();


    // NB: we could just wait for the network to go out of scope, but then this would happen
    //     after the call to essentia::shutdown() where the FFTW structs would already have
    //     been freed, so let's just delete everything explicitly now
    n.clear();

    delete output;


    return pool;
}


int Extractor::compute_all_information(std::string audioFilename, std::string profileFilename) {
    int result;
    try {
        MusicExtractor *extractor = new MusicExtractor();

        extractor->setExtractorOptions(profileFilename);
        extractor->mergeValues(extractor->results);

        result = extractor->compute(audioFilename);

        if (result > 0) {
            cerr << "Quitting early." << endl;
        } else {
            extractor->outputToFile(extractor->stats, audioFilename + "_result.json");
            if (extractor->options.value<Real>("outputFrames")) {
                extractor->outputToFile(extractor->results, audioFilename + "_frames");
            }
        }
    }
    catch (EssentiaException &e) {
        cout << e.what() << endl;
        return 1;
    }
    return result;
}


essentia::Pool Extractor::compute_fades_information(std::string name) {

    // parameters:
    int sr = 44100;
    int framesize = sr / 4;
    int hopsize = 256;
    Real frameRate = Real(sr) / Real(hopsize);

    standard::AlgorithmFactory &factory = standard::AlgorithmFactory::instance();

    standard::Algorithm *audio = factory.create("MonoLoader",
                                                "filename", name,
                                                "sampleRate", sr);

    standard::Algorithm *frameCutter = factory.create("FrameCutter",
                                                      "frameSize", framesize,
                                                      "hopSize", hopsize);

    standard::Algorithm *rms = factory.create("RMS");

    standard::Algorithm *fadeDetect = factory.create("FadeDetection",
                                                     "minLength", 0.01,
                                                     "cutoffHigh", 0.6,
                                                     "cutoffLow", 0.4,
                                                     "frameRate", frameRate);

    // create a pool for fades' storage:
    Pool pool;

    // set audio:
    vector<Real> audio_mono;
    audio->output("audio").set(audio_mono);

    cout << "-------- connecting algos (Fades) --------" << endl;
    // set frameCutter:
    vector<Real> frame;
    frameCutter->input("signal").set(audio_mono);
    frameCutter->output("frame").set(frame);

    // set rms:
    Real rms_value;
    rms->input("array").set(frame);
    rms->output("rms").set(rms_value);

    // we need a vector to store rms values:
    std::vector<Real> rms_vector;

    cout << "-------- start retrieving Fades information on : " << name << " --------" << endl;
    // load audio:
    audio->compute();

    // compute and store rms first and will compute fade detection later:
    while (true) {
        frameCutter->compute();
        if (frame.empty())
            break;

        rms->compute();
        rms_vector.push_back(rms_value);
    }


    // set fade detection:
    array2d fade_in;
    array2d fade_out;
    fadeDetect->input("rms").set(rms_vector);
    fadeDetect->output("fadeIn").set(fade_in);
    fadeDetect->output("fadeOut").set(fade_out);

    // compute fade detection:
    fadeDetect->compute();


    // Exemplifying how to add/retrieve values from the pool in order to output them  into stdout
    if (fade_in.dim1()) {
        AddToPool(fade_in, "high_level.fade_in", pool);
        vector<vector<Real> > fadeIn = pool.value<vector<vector<Real> > >("high_level.fade_in");
        cout << "fade ins: ";
        for (size_t i = 0; i < fadeIn.size(); ++i) {
            //std::string  name_to_store= "high_level.fade_in.n_" + itoa(i);
            //pool.add(name_to_store, fadeIn[i]);
            cout << fadeIn[i] << endl;
        }
    }
    else cout << "No fades in found" << endl;

    if (fade_out.dim1()) {
        AddToPool(fade_out, "high_level.fade_out", pool);
        vector<vector<Real> > fadeOut = pool.value<vector<vector<Real> > >("high_level.fade_out");
        cout << "fade outs: ";
        for (size_t i = 0; i < fadeOut.size(); ++i)
            cout << fadeOut[i] << endl;
    }
    else cout << "No fades out found" << endl;


    // aggregate the results
    Pool aggrPool; // the pool with the aggregated MFCC values
    const char *stats[] = {"mean", "var", "min", "max", "cov"};

    standard::Algorithm *aggr = standard::AlgorithmFactory::create("PoolAggregator",
                                                                   "defaultStats", arrayToVector<string>(stats));

    aggr->input("input").set(pool);
    aggr->output("output").set(aggrPool);
    aggr->compute();


    string output_name(HL_DIR);
    output_name.append(Utils::extract_filename(name));
    output_name.append("_fades.yaml");

    // write results to file
    cout << "-------- writing ALL results to file " << output_name << " --------" << endl;

    standard::Algorithm *output = standard::AlgorithmFactory::create("YamlOutput",
                                                                     "filename", output_name);
    output->input("pool").set(aggrPool);
    output->compute();


    delete audio;
    delete frameCutter;
    delete rms;
    delete fadeDetect;

    return pool;
}


vector<vector<Real>> Extractor::compute_phrases_from_fades(std::string name){
    // parameters:
    int sr = 44100;
    int framesize = sr / 4;
    int hopsize = 256;
    Real frameRate = Real(sr) / Real(hopsize);

    standard::AlgorithmFactory &factory = standard::AlgorithmFactory::instance();

    standard::Algorithm *audio = factory.create("MonoLoader",
                                                "filename", name,
                                                "sampleRate", sr);

    standard::Algorithm *frameCutter = factory.create("FrameCutter",
                                                      "frameSize", framesize,
                                                      "hopSize", hopsize);

    standard::Algorithm *rms = factory.create("RMS");

    standard::Algorithm *fadeDetect = factory.create("FadeDetection",
                                                     "minLength", 0.01,
                                                     "cutoffHigh", 0.6,
                                                     "cutoffLow", 0.4,
                                                     "frameRate", frameRate);

    vector<vector<Real>> output;

    // set audio:
    vector<Real> audio_mono;
    audio->output("audio").set(audio_mono);

    cout << "-------- connecting algos (Fades) --------" << endl;
    // set frameCutter:
    vector<Real> frame;
    frameCutter->input("signal").set(audio_mono);
    frameCutter->output("frame").set(frame);

    // set rms:
    Real rms_value;
    rms->input("array").set(frame);
    rms->output("rms").set(rms_value);

    // we need a vector to store rms values:
    std::vector<Real> rms_vector;

    cout << "-------- start retrieving Fades information on : " << name << " --------" << endl;
    // load audio:
    audio->compute();

    // compute and store rms first and will compute fade detection later:
    while (true) {
        frameCutter->compute();
        if (frame.empty())
            break;

        rms->compute();
        rms_vector.push_back(rms_value);
    }


    // set fade detection:
    array2d fade_in;
    array2d fade_out;
    fadeDetect->input("rms").set(rms_vector);
    fadeDetect->output("fadeIn").set(fade_in);
    fadeDetect->output("fadeOut").set(fade_out);

    // compute fade detection:
    fadeDetect->compute();


    if (fade_in.dim1() && fade_out.dim1()) {
        cout << "fade ins: ";
        // assume same size
        vector<Real> to_push;
        for (size_t i = 0; i < (size_t)min(fade_in.dim1(), fade_out.dim1()); ++i) {
            to_push.push_back(fade_in[i][IN]);
            to_push.push_back(fade_out[i][OUT]);
            output.push_back(to_push);
            to_push.clear();
        }
    }
    else cout << "Empty fades" << endl;

    return output;
}


int Extractor::compute_lowlevel_information(std::string audioFilename,
                                            std::string lowLevelJsonOutput,
                                            std::string profileFilename) {
    // Returns: 1 on essentia error
    //          2 if there are no tags in the file
    int result;
    try {

        lowLevelJsonOutput = LL_DIR + lowLevelJsonOutput + ".json";

        if(Utils::file_exist(lowLevelJsonOutput)){
            cout << "Low Level information already computed for " << Utils::extract_filename(lowLevelJsonOutput) << endl;
            return 0;
        }
        MusicExtractor *extractor = new MusicExtractor();

        extractor->setExtractorOptions(profileFilename);
        extractor->mergeValues(extractor->results);

        result = extractor->compute(audioFilename);

        if (result > 0) {
            cerr << "Quitting early." << endl;
        } else {
            extractor->outputToFile(extractor->stats, lowLevelJsonOutput);
            if (extractor->options.value<Real>("outputFrames")) {
                extractor->outputToFile(extractor->results, lowLevelJsonOutput+"_frames");
            }
        }
        delete extractor;
    }
    catch (EssentiaException& e) {
        cout << e.what() << endl;
        return 1;
    }
    return result;
}

essentia::Pool Extractor::compute_highlevel_information(std::string highLevelJsonInput,
                                             std::string highLevelJsonOutput,
                                             std::string profileFilename){

    Pool pool;
    try{
        cout.precision(10); // TODO ????

        highLevelJsonInput = LL_DIR + highLevelJsonInput + ".json";
        highLevelJsonOutput = HL_DIR + highLevelJsonOutput + ".json";

        if(Utils::file_exist(highLevelJsonOutput)){
            cout << "High Level information already computed for " << Utils::extract_filename(highLevelJsonOutput) << endl;
            essentia::standard::AlgorithmFactory &factory = essentia::standard::AlgorithmFactory::instance();
            essentia::standard::Algorithm *yamlInput = factory.create("YamlInput",
                                                                      "filename", highLevelJsonOutput,
                                                                      "format", "json");
            yamlInput->output("pool").set(pool);
            yamlInput->compute();

            return pool;
        }

        MusicExtractor *extractor = new MusicExtractor();
        extractor->setExtractorOptions(profileFilename);

        string format = extractor->options.value<string>("highlevel.inputFormat");
        if (format != "json" && format != "yaml") {
            cerr << "incorrect format specified: " << format << endl;
            return pool;
        }

        // load descriptor file
        essentia::standard::AlgorithmFactory &factory = essentia::standard::AlgorithmFactory::instance();
        essentia::standard::Algorithm *yamlInput = factory.create("YamlInput",
                                                                  "filename", highLevelJsonInput,
                                                                  "format", format);
        yamlInput->output("pool").set(pool);
        yamlInput->compute();

        // apply SVM models and save
        extractor->computeSVMDescriptors(pool);

        pool.removeNamespace("lowlevel");
        pool.removeNamespace("rhythm");
        pool.removeNamespace("tonal");

        vector<string> keys = pool.descriptorNames("metadata.version");
        for (int i = 0; i < (int) keys.size(); ++i) {
            string k = keys[i];
            string newk = "metadata.version.lowlevel." + k.substr(17);
            pool.set(newk, pool.value<string>(keys[i]));
            pool.remove(keys[i]);
        }

        pool.set("metadata.version.highlevel.essentia", essentia::version);
        pool.set("metadata.version.highlevel.essentia_git_sha", essentia::version_git_sha);
        pool.set("metadata.version.highlevel.extractor", EXTRACTOR_VERSION);
        pool.set("metadata.version.highlevel.gaia", gaia2::version);
        pool.set("metadata.version.highlevel.gaia_git_sha", gaia2::version_git_sha);

        extractor->mergeValues(pool);
        extractor->outputToFile(pool, highLevelJsonOutput);
        cout << "output :" << highLevelJsonOutput << endl;

        delete extractor;
    }
    catch (EssentiaException &e) {
        cout << e.what() << endl;
        return pool;
    }
    return pool;
}
