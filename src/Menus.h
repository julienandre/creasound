//
// Created by user on 06/06/16.
//

#ifndef CREASOUND_MENUS_H
#define CREASOUND_MENUS_H

#include <string>
#include "Config.h"

class Menus {
public:
    static int menu_main();
    static void menu_playlists();
    static void menu_generate();
    static void menu_parameters();
    static Config* load_config();
    static std::string select_playlist();
};

#endif //CREASOUND_MENUS_H
