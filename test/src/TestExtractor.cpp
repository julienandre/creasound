//
// Created by SARROCHE on 08/06/16.
//
#include "TestExtractor.h"
#include "../../src/Extractor.h"
#include <stdlib.h>
#include <iostream>
#include <unistd.h>


using namespace std;
using namespace essentia;


void TestExtractor::test_all() {
    std::cout << "\n--- Starting Extractor tests" << std::endl;
    TestExtractor::test_compute_tonal();
    TestExtractor::test_compute_rhythm();
    std::cout << "--- End  of  Extractor tests" << std::endl;
}


void TestExtractor::test_compute_tonal(){
    std::cout << "------ Test tonal start " << std::endl;
    essentia::init();
    essentia::Pool resultat = Extractor::compute_tonal_information("test_audio/test.mp3");
    essentia::shutdown();

    string filename = "test_audio/test.mp3.tonal.yaml";
    bool file_exists = access(filename.c_str(), F_OK) != -1 ;
    assert(file_exists);

    assert(resultat.contains<std::string>("tonal.keys"));
    assert(resultat.contains<std::string>("tonal.key_scale"));
    assert(resultat.contains<std::string>("tonal.chords_key"));
    std::cout << "------ Test tonal done " << std::endl;

}

void TestExtractor::test_compute_rhythm(){
    std::cout << "------ Test rhythm start " << std::endl;
    essentia::init();
    essentia::Pool resultat = Extractor::compute_rythm_information("test_audio/test.mp3");
    essentia::shutdown();

    string filename = "test_audio/test.mp3.rhythm.yaml";
    bool file_exists = access(filename.c_str(), F_OK) != -1 ;
    assert(file_exists);

    assert(resultat.contains<Real>("rhythm.bpm"));
    assert(resultat.contains<vector<Real>>("rhythm.bpmIntervals"));
    assert(resultat.contains<vector<Real>>("rhythm.estimates"));
    assert(resultat.contains<vector<Real>>("rhythm.ticks"));
    std::cout << "------ Test rhythm done " << std::endl;

}
