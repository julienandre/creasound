//
// Created by user on 08/06/16.
//

#ifndef CREASOUND_TESTEXTRACTOR_H_H
#define CREASOUND_TESTEXTRACTOR_H_H

class TestExtractor {

private:
    static void test_compute_tonal();
    static void test_compute_rhythm();
public:
    static void test_all();
};

#endif //CREASOUND_TESTEXTRACTOR_H_H
