//
// Created by user on 08/06/16.
//

#ifndef CREASOUND_TESTMUSICFACTORY_H
#define CREASOUND_TESTMUSICFACTORY_H


class TestMusicFactory {

private:
    static void test_create_music();
public:
    static void test_all();
};


#endif //CREASOUND_TESTMUSICFACTORY_H
