//
// Created by SARROCHE on 08/06/16.
//
#include "TestExtractor.h"
#include "TestMusicFactory.h"
#include <iostream>

int main(){

    std::cout << "UNIT TESTING STARTING \n" << std::endl;
    TestExtractor::test_all();
    TestMusicFactory::test_all();
    std::cout << "\nUNIT TESTING ENDING" << std::endl;

}