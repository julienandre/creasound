//
// Created by user on 08/06/16.
//

#include "TestMusicFactory.h"
#include "../../src/MusicFactory.h"
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <vector>

using namespace std;
using namespace essentia;

std::streambuf *cinbuf;
static std::ifstream in;


void redirect_in(bool save);
void reset_in();



void TestMusicFactory::test_all() {
    std::cout << "\n--- Starting Factory tests" << std::endl;
    TestMusicFactory::test_create_music();
    std::cout << "--- End  of  Factory tests" << std::endl;
}

void TestMusicFactory::test_create_music() {
    std::cout << "------ Test Create music start " << std::endl;

    // first attempt not saving  the result
    // redirect CIN to not save the result
    redirect_in(false);
    // start music generation
    MusicFactory::createMusic("test_audio/test.mp3", "test_audio/test.mp3",
                              "result.mp3", MF_ALGO_MERGE);

    reset_in();

    string filename = "test_audio/result.mp3";
    // check if file exists
    bool file_exists = access(filename.c_str(), F_OK) != -1 ;
    assert(not file_exists);

    // second attempt saving MP3 result in test_audio
    // redirect CIN to save the result
    redirect_in(true);
    // start music generation
    MusicFactory::createMusic("test_audio/test.mp3", "test_audio/test.mp3",
                              "result.mp3", MF_ALGO_MERGE);

    reset_in();

    // check if file exists
    file_exists = access(filename.c_str(), F_OK) != -1 ;
    assert(file_exists);

    std::cout << "------ Test Create music done " << std::endl;
}

void redirect_in(bool save){
    if(save)
        in.open("input/save.txt");
    else
        in.open("input/no_save.txt");
    //save old buf
    cinbuf = std::cin.rdbuf();
    //redirect std::cin to in.txt!
    std::cin.rdbuf(in.rdbuf());

}

void reset_in(){
    std::cin.rdbuf(cinbuf);   //reset to standard input again
}