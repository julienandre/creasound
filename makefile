# Makefile
# Project Creasound
#

CFLAGS=-g -pipe -Wall -O2 -fPIC -I/usr/local/include/essentia/ -I/usr/local/include/essentia/scheduler/ -I/usr/local/include/essentia/streaming/  -I/usr/local/include/essentia/utils -I/usr/include/taglib -I/usr/local/include/gaia2 -I/usr/include/qt4 -I/usr/include/qt4/QtCore -I -D__STDC_CONSTANT_MACROS -I/usr/lib/sfml/include -I/usr/lib/soundtouch/include -I/usr/lib/soundtouch/src/soundtouch
LIBS=-L/usr/local/lib -lessentia -lfftw3 -lyaml -lavcodec -lavformat -lavutil -lsamplerate -ltag -lfftw3f -lQtCore -lgaia2 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lSoundTouch 

OUT=--outdir=./bin/
DEPS=./src/*.h ./src/soundstretch/*.h

CPP=$(wildcard ./src/*.cpp)
CPP+=$(wildcard ./src/soundstretch/*.cpp)
CPP+=$(wildcard ./src/extractor_music/*.cpp)
OBJ=$(CPP:./src/%.cpp=./obj/%.o)
OUT=creasound

obj/%.o: src/%.cpp $(DEPS)
	@mkdir -p obj/soundstretch
	@mkdir -p obj/extractor_music
	@echo $<
	@g++ -std=c++11 -c -o $@ $< $(CFLAGS)
	
main: $(OBJ)
	@mkdir -p config
	@mkdir -p generated/playlists
	@mkdir -p generated/descriptors/lowlevel
	@mkdir -p generated/descriptors/highlevel
	@mkdir -p generated/audio
	@mkdir -p generated/database
	@g++ $(CFLAGS) -o $(OUT) $(OBJ) $(LIBS)

clean:
	rm -rf ./obj *.yaml *.json creasound ./generated/
	find -name *~ -delete
